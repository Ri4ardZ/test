package com.example.endpoint.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class IncrementService {
    private volatile int incrementBy = 1;

    public long[] increment(long[] numbers) {
        int increment = incrementBy;

        return Arrays.stream(numbers)
                .map(number -> number + increment)
                .toArray();
    }

    public void setIncrementBy(int incrementBy) {
        this.incrementBy = incrementBy;
    }
}
