package com.example.endpoint.api.json;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class IncrementRequest {
    @NotNull
    @Min(1)
    private int incrementBy;

    public Integer getIncrementBy() {
        return incrementBy;
    }

    public void setIncrementBy(int incrementBy) {
        this.incrementBy = incrementBy;
    }
}
