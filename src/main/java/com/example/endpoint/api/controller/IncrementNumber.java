package com.example.endpoint.api.controller;

import com.example.endpoint.api.json.IncrementRequest;
import com.example.endpoint.service.IncrementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api")
public class IncrementNumber {
    private final IncrementService service;

    public IncrementNumber(IncrementService service) {
        this.service = service;
    }

    @GetMapping("/increment")
    public ResponseEntity<String> incrementEachNumber(@RequestParam("numbers") String numbers) {
        try {
            long[] longs = Arrays.stream(numbers.trim().split(",")).mapToLong(Long::valueOf).toArray();

            String result = Arrays.stream(service.increment(longs))
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(","));

            return ok(result);
        } catch (NumberFormatException e) {
            return status(HttpStatus.BAD_REQUEST).build();
        }
    }


    @PostMapping("/setIncrementBy")
    public ResponseEntity<Object> setIncrementBy(@RequestBody @Valid IncrementRequest request) {
        service.setIncrementBy(request.getIncrementBy());

        return ok().build();
    }
}
