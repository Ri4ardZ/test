package com.example.endpoint.api.controller;

import com.example.endpoint.api.json.IncrementRequest;
import com.example.endpoint.service.IncrementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(IncrementNumber.class)
@RunWith(SpringRunner.class)
public class IncrementNumberTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @MockBean
    IncrementService service;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void incrementEachNumber_successful() throws Exception {
        when(service.increment(new long[]{1, 2, 3})).thenReturn(new long[]{2, 3, 4});

        mockMvc.perform(get("/api/increment")
                .param("numbers", "1,2,3"))
                .andExpect(status().isOk())
                .andExpect(content().string("2,3,4"));
    }

    @Test
    public void incrementEachNumber_badRequest() throws Exception {
        mockMvc.perform(get("/api/increment")
                .param("numbers", "1,a,3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setIncrementBy_successful() throws Exception {
        IncrementRequest request = new IncrementRequest();
        request.setIncrementBy(2);

        mockMvc.perform(post("/api/setIncrementBy")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void setIncrementBy_notValidRequest() throws Exception {
        IncrementRequest request = new IncrementRequest();
        request.setIncrementBy(0);

        mockMvc.perform(post("/api/setIncrementBy")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request)))
                .andExpect(status().isBadRequest());
    }
}