package com.example.endpoint.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class IncrementServiceTest {
    @Autowired
    IncrementService subj;


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void increment() {
        long[] expected = new long[] {2,3,4};
        long[] real = subj.increment(new long[]{1, 2, 3});

        assertArrayEquals(expected, real);
        assertNotNull(real);
    }
}